ARG port
FROM node:alpine3.13

RUN mkdir -p /tsunami/tsunami_node

COPY config.json /tsunami/
COPY yarn.lock package.json server.js /tsunami/tsunami_node/

EXPOSE ${port}

WORKDIR "/tsunami/tsunami_node"

RUN yarn install

CMD cd /tsunami/tsunami_node
ENTRYPOINT node server.js


