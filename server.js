const Discord = require('discord.js');
const client = new Discord.Client({ 
	retryLimit: Infinity,
	intents: [32768]
});
const express = require('express');
const webapp = express();
const path = require('path');
const webserver = require('http').Server(webapp);
const fetch = require('node-fetch');
const fs = require('fs');

const MAX_LOG = 100;

//Authorize URL for new BOTS https://discordapp.com/oauth2/authorize?&client_id=CLIENTID&scope=bot&permissions=67584
//(prod) Jabberwock token NTk3NDQzNjg4ODg0NDY5Nzc2.XSIL0A.VREivKzppt5IutSWQi-yiBzzMnI
//(test) tsu_test token NTk3Mjg1NDA4ODQ5NTI2ODA5.XSF3eA.uZ9AmfhMEiYX2o2fOPq46HKVnqA
//krule test server MTA4MDY1MTIzNTI3MjY5OTkwNA.GOp3js.Xo1mnQxgFVNNImeYC_9ri0HU_I1smf08uKg6UI

const file = "/data/registration.json";
const configfile = "/tsunami/config.json";

//note to krule, use port 8004 for local testing
const config = {
    "bot_token":"NTk3Mjg1NDA4ODQ5NTI2ODA5.XSF3eA.uZ9AmfhMEiYX2o2fOPq46HKVnqA"
    ,"mud_url":"http://tsunami:8004/discord_message"
    ,"port":8002
    ,"debug":true
}

let connected = false; //Indicate whether a valid Discord connection exists

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


try{
	var tconfig = JSON.parse( fs.readFileSync(configfile) );
	for(var key in config){
		if(tconfig.hasOwnProperty(key) && config.hasOwnProperty(key)){
			config[key] = tconfig[key];
		}
	}
} catch (e){

}

try{
	var tconfig = process.env;
	for(var key in config){
		if(tconfig.hasOwnProperty(key) && config.hasOwnProperty(key)){
			config[key] = tconfig[key];
		}
	}
} catch (e){

}

console.log(config);

let logarray = [];
function log(val){
	console.log((new Date()).toUTCString() + ": " + val);
	logarray = [(new Date()).toUTCString() + ": " + val, ...logarray.slice(0, MAX_LOG)];
}

function save(){
	try{
		fs.writeFileSync(file,JSON.stringify(registration));
	}catch(e){
		log("There is an error saving the registration file");	
	}	
}

function isverified_by_discordid(discordid){
	var flag = false;
	for(var i = registration.verified.length - 1; i>=0; i--){
		var a = registration.verified[i];
		if(a.id == discordid){
			flag = true;
			break;
		}
	}
	return flag;
}

function isverified_by_mudname(name){
	var flag = false;
	for(var i = registration.verified.length - 1; i>=0; i--){
		var a = registration.verified[i];
		if(a.mudname == name){
			flag = true;
			break;
		}
	}
	return flag;
}

function clean_by_discordid(discordid){
	for(var i = registration.verified.length - 1; i>=0; i--){
		var a = registration.verified[i];
		if(a.id == discordid){
			registration.verified.splice(i,1);
			break;
		}
	}
}

function clean_by_mudname(name){
	for(var i = registration.verified.length - 1; i>=0; i--){
		var a = registration.verified[i];
		if(a.mudname == name){
			registration.verified.splice(i,1);
			break;
		}
	}
}

function get_user_by_discordid(discordid){
	for(var i = registration.verified.length - 1; i>=0; i--){
		var a = registration.verified[i];
		if(a.id == discordid){
			return a;
		}
	}
}

var registration = {
	"pending":{}
	,"verified":[]
}

if(!fs.existsSync(file)){
	save();
} else {
	registration = JSON.parse(fs.readFileSync(file));
}

if( !Array.isArray(registration.verified) ){
	registration.verified = [];
}

//reset any pending since its a fresh restart
registration.pending = {};

console.log(registration);

client.on('ready', () => {
	log(`Logged in as ${client.user.tag}!`);  
	connected = true;
});

//discord client to handle discord->mud
client.on('message', async (msg) => {
	var resmessage = 'No message returned';
	try{		
		//if this is a bot, then it is a circular message from tsunami
		if(msg.author.bot)
			return;
		
		//check user registration		
		var isregrequest = registration.pending.hasOwnProperty(msg.content);
		
		// handle user linking
		if(isregrequest){
			resmessage = `The user ${msg.author.username} is validating ${registration.pending[msg.content]}`;
			var username = registration.pending[msg.content];
			var user = {
				"id":msg.author.id,
				"mudname":username,
				"discordname":msg.author.name,
				"discriminator":msg.author.discriminator
			}
			delete registration.pending[msg.content];
			clean_by_discordid(user.id);
			registration.verified.push(user);
			save();		
			resmessage = `The user ${msg.author.username} is REGISTERED to ${username}`;	
		} else {
			var isregistered = isverified_by_discordid(msg.author.id);	
			
			//if this is a player, then it is coming from discord	
			//get name
			var username = msg.author.username + "#" + msg.author.discriminator;
			if(isregistered){
				var regobj = get_user_by_discordid(msg.author.id);
				username = regobj.mudname;
			}
			//get message
			var message = msg.content;		
			//get channel
			var channelname = "chat";
			var type = "dm";
			//console.log(msg.channel.type);
			if(msg.channel.type != "dm"){
				channelname = msg.channel.name.toLowerCase();
				type = "chat";	
			}
			//if validation checks out, send it to the mud	
			if(username && channelname && message){
				resmessage = `The user ${msg.author.username} is sending a message as ${username}`;
				
				// Changing to node-fetch as request is depreciated.
				let result = await fetch(`${config.mud_url}?type=${type}&uid=${msg.author.id}&username=${ encodeURIComponent(capitalizeFirstLetter(username)) }&channelname=${channelname}&message=${ encodeURIComponent(message) }`);
				if (!result.ok) 
					log(await result.text());
				else
					log("Message delivered to the MUD");
			} else {
				log("Failed to send! User:" + username + "  Channel:" + channelname + "  Message:" + message);
			}
		}
	} catch(e){
		resmessage = e.message;	
	}
	log(resmessage);
});

//connect to discord
client.login(config.bot_token);

//test send to tsunami
webapp.get('/send_mud_test', async (req, res)=>{
	var resmessage = 'SENDING TEST TO THE MUD';
	try{	
		let result = await fetch(`${config.mud_url}?username=sid&channelname=chat&message=hello+world`);
		if (!result.ok) 
			log(await result.text());
		else
			log("Message delivered to the MUD");
		/*	
		request(`${config.mud_url}?username=sid&channelname=chat&message=hello+world`, { }, (err, res, body) => {
		  if (err) 
			  log(err.message);
		  else
			  log("MUD TEST MESSAGE DELIVERED");
		});
		*/
	} catch(e){
		resmessage = "ERROR: " + e.message;		
	}
	log(resmessage);
	res.send(resmessage);
});

//return a log to help get info back
webapp.get('/read_registration', function (req, res) {
	var resmessage = 'No message returned';
	try{		
		resmessage = JSON.stringify(registration);
	} catch(e){
		resmessage = "ERROR: " + e.message;		
	}
	res.send(resmessage);
});

//return a log to help get info back
webapp.get('/read_log', function (req, res) {
	return res.status(200).send(logarray);
});

//express webserver to handle mud->discord
webapp.get('/discord_message', async function (req, res) {
	var resmessage = 'No message returned';
	try{
		if(!connected) throw(new Error("Discord Client Connecting..."));
		var isverificationrequest = req.query.hasOwnProperty("secret");
		
		if(isverificationrequest){
			//get url params
			var username = req.query.username;
			var secret = req.query.secret;
			
			//registration			
			registration.pending[secret] = username;
			
			//save
			save();		

			//set response message
			resmessage = `Verification was added: ${secret}:${username}`;
		} else{	//THIS IS NOT A VERIFICATION REQUEST			
			//get url parameters
			var username = req.query.username;
			var channelname = req.query.channelname;
			var message = req.query.message;
			var thischannel;
			
			resmessage = "User cannot send messages because they are not verified";
			
			//if( isverified_by_mudname(username) ){
				//todo check this username to find which name to send as?
			//}
				
			//Checking for channels - if not found try to reconnect - Orion 4-Jun-2021
			while(!client.channels || !client.channels.cache || !client.channels.cache.size){
				log("We are not connected to discord and attempting to reconnect.");
				connected = false;
				await client.login(config.bot_token);
			}

			//find the discord channel
			if(client && client.channels && client.channels.cache){
				if( !isNaN(parseFloat(channelname)) && isFinite(channelname) ){
					//channelname is an id, so its an PM
					//krule 525956797903798272
					//krule test channel 931405061567967282
					client.users.cache.forEach(function(u){
						if(channelname && u.id == channelname)
							thischannel = u;
					});
				} else {
					client.channels.cache.forEach(function(c){
						if(c && c.name && channelname && c.name.toLowerCase() == channelname.toLowerCase())
							thischannel = c;
					});
				}
			}
			//if we found the channel, send the message
			if(thischannel){					
				thischannel.send(`*${username} ${message}`);
				resmessage = "The message was delivered";
			}
			else{
				resmessage = "The channel was not found to deliver the message to";
			}
		}
	} catch(e){
		resmessage = "ERROR:" + e.message;		
	}
	log(resmessage);
	res.send(resmessage);
});
//start the server
webserver.listen(config.port, function () {
	log('Discord Messager is listening on ' + webserver.address().port);
});  

/* PROXY SERVER TO REDIRECT REQUESTS FROM THIS END-POINT FROM HTTP TO HTTPS
   AND RETURN RESULTS BACK TO ORIGIN
*/
webapp.get('/https_proxy', async (req, res)=>{
	if(!req.query || !req.query.url) return res.status(400).send("No URL in request");
	let result;
	try{
		result = await fetch(req.query.url);
		
		if(!result.ok){
			log(`Proxy Error for ${req.query.url}: ${result.status} : ${await result.text()}`);
			return res.status(result.status).send(body);
		}

		const contentType = result.headers.get("content-type");
		if (contentType && contentType.indexOf("application/json") == -1) { //text response
			return res.status(200).send(await result.text());
		}
		else{ //json response
			return res.status(200).json(await result.json());
		}
	}
	catch(e){
		log(`Proxy Error for ${req.query.url}: ${e.message}`);
		return res.status(500).send(e.message);
	}
});

